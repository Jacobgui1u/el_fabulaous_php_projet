<?php
namespace wishlist\controleur;
use wishlist\vue\Vue;
use wishlist\model\Membre;
use wishlist\model\Liste;
use wishlist\model\Item;
/**
* classe qui gere les controle de l'affichage
*/
class ControleurHtml{

  /**
  * methode qui va afficher la page d'index du site
  */
  public function acceuil(){
    $vI=new Vue();
    if(isset($_SESSION['login'])){
      $result = Membre::select('*')->where( 'login','=',$_SESSION['login'])->get();
      // si on obtient une réponse, alors l'utilisateur est un membre
      if (count($result)==1) {

      }else{
        $_SESSION['login']=null;
      }
    }
    $vI->render(1);
  }


  private function deconnexion(){
    if (isset($_POST['deconnexion']) && $_POST['deconnexion'] == 'Deconnexion') {
      header('Location: ');
      // On détruit les variables de notre session
      $_SESSION['login']=null;
      exit();
    }
  }

  private function connexion($vI,$rendu){
    $_SESSION['erreur']=null;
    // on teste si le visiteur a soumis le formulaire de connexion
    if (isset($_POST['connexion']) && $_POST['connexion'] == 'Connexion') {
      // si les informations sont correcte, on ce connecte
      if ((isset($_POST['login']) && !empty($_POST['login'])) && (isset($_POST['pass']) && !empty($_POST['pass']))) {
        $liste = Membre::select('*')->where( 'login','=',$_POST['login'])->get();
        $result=array();
        foreach ($liste as $mdp ) {
          if(password_verify($_POST['pass'],$mdp['mdpCrypt'])){
            array_push($result,$mdp);
            break;
          }
        }

        // si on obtient une réponse, alors l'utilisateur est un membre
        if (count($result)==1) {
          /*
          $token=openssl_random_pseudo_bytes(32);
          $token=bin2hex($token);
          $_SESSION['token']=$token;
          */
          $_SESSION['login'] = $_POST['login'];
          //redirection vers un autre trucs
          header('Location: ');
          exit();

          // si on ne trouve aucune réponse, le visiteur s'est trompé soit dans son login, soit dans son mot de passe
          //et on reaffiche la vue de l'index avec un erreur
        }elseif (count($result)== 0) {
          $_SESSION['erreur'] = 'Compte non reconnu.';
          $vI->render($rendu);
        }else {
          $_SESSION['erreur']= 'Problème dans la base de données : plusieurs membres ont les mêmes identifiants de connexion.';
          $vI->render($rendu);
        }
      }else {
        $_SESSION['erreur']= 'Au moins un des champs est vide.';
        $vI->render($rendu);
      }
    }
  }





  /**
  * methode qui va traiter les informations entrée par l'utilisateu
  */
  public function acceuilControle(){
    $vI=new Vue();
    //veut on une deconnexion?
    $this->deconnexion();
    //veut on ce connecter?
    $this->connexion($vI,1);
    //veut on une nouvelle liste
    $this->nouvelleListe();
  }




  public function listeActive(){
      $vI=new Vue();
      if (isset($_POST['liste1']) && $_POST['liste1'] == 'l1') {
          header('Location: op.php');
          exit();
      }
  }




  public function AfficheInscription(){
    $vI=new Vue();
    $vI->render(3);
  }

  /**
  * methode qui permet l'inscription a partir des informations donnée dans la page d'inscription
  */
  public function inscription(){
    $_SESSION['erreur']=null;
    //create table membre( id_membre int NOT NULL AUTO_INCREMENT, login varchar(32), mdpCrypt varchar(255),primary key(id_membre));
    // on teste si le visiteur a soumis le formulaire
    if (isset($_POST['inscription']) && $_POST['inscription'] == 'Inscription') {
      // on teste l'existence de nos variables. On teste également si elles ne sont pas vides
      if ((isset($_POST['login']) && !empty($_POST['login'])) && (isset($_POST['pass']) && !empty($_POST['pass'])) && (isset($_POST['pass_confirm']) && !empty($_POST['pass_confirm']))) {

        /* on testera plus tard si il y a des caractere non voulue */

        // on teste les deux mots de passe
        if ($_POST['pass'] != $_POST['pass_confirm']) {
        $_SESSION['erreur'] = 'Les 2 mots de passe sont différents.';
        }else {

          // on teste si une entrée de la base contient ce couple login / pass
          $result = Membre::select('*')->where( 'login','=',$_POST['login'])->get();
          $result=array();

          //si personne n'à ce login, on créer un nouveau membre
          if (count($result) == 0) {
            //on créer le membre avec un mdp stocké et crypté par password_hash
            $mb=['login'=>$_POST['login'],'mdpCrypt'=>password_hash($_POST['pass'],PASSWORD_BCRYPT)];
            Membre::insert($mb);
            header('Location: ../wishlist');
            exit();
          }else {
            $_SESSION['erreur'] = 'Un membre possède déjà ce login.';
          }
        }
      }else {
        $_SESSION['erreur'] = 'Au moins un des champs est vide.';
      }
    }
  }



  /*
  *
  *
  public function formRecup(){
    //donnée posté?
    //vérifier que la valeur de l'input ou du bouton de validation
    //est bien présente dans les donnée reçue
    //test si valider_inc existe et si valider_inc egale a valide_f1
    try{
      $aut=new Authentification();
      $aut->authenticate();
      $aut->loadProfile($_POST['pseudo_inc']);

    }catch(AuthException $ae){
        echo "bad login name or passwd<br>";
    }
  }*/





  /**
  * methode privé qui créer un token aleatoire
  */
  private function randToken(){
    $token=openssl_random_pseudo_bytes(32);
    $token=bin2hex($token);
    return $token;
  }



}
