<?php
namespace wishlist\controleur;
use wishlist\vue\Vue;
use wishlist\model\Membre;
use wishlist\model\Liste;
use wishlist\model\Item;

class ControleurItem{


  /**
  * fonction qui ajoute un item dans la base de donnée
  */
  private function nouveauItem($token){
    //: nom et description et prix et url
    $idL=Liste::select('*')->get();
    foreach ($idL as $mdp ) {
      if(password_verify($token,$mdp['token'])){
        $idL=$mdp;
        break;
      }
    }
    if(isset($_post['url'])){
      $it=['liste_id'=>$idL['no'] ,'nom'=>$_POST['nom'], 'descr'=>$_POST['description'], 'tarif'=>$_POST['prix'], 'url'=>$_POST['url']];
      $it=Item::insert($it);
    }else{
      $it=['liste_id'=>$idL['no'] ,'nom'=>$_POST['nom'], 'descr'=>$_POST['description'], 'tarif'=>$_POST['prix'], 'url'=>null];
      $it=Item::insert($it);
    }
  }

  /**
  * methode qui ajoute un item dans la liste courante
  */
  public function ajoutItem($token){
    $max=0;
    $_SESSION['erreur']=null;
    $idIt=null;
    $vI=new Vue();
    $vI->tokenCourant($token);
    $a=Item::select('*')->get();


    if (isset($_POST['ajouter']) && $_POST['ajouter'] == 'Ajouter') {
      if ((isset($_POST['nom']) && !empty($_POST['nom'])) && (isset($_POST['description']) && !empty($_POST['description']))&& (isset($_POST['prix']) && !empty($_POST['prix']))) {
        $this->nouveauItem($token);
        foreach($a as $i){
          $idIt=$i['id'];
        }
        if(isset($idIt)){
          $_post['ajouter']=null;
          header('Location:../'.$token.'/item/'.$idIt);exit();
        }else{
          $_post['ajouter']=null;
          $_SESSION['erreur'] = 'erreur de creation, pas d\'id généré.';$vI->render(6);exit();
        }
        //on affiche le formulaire sans balise html
      }else{$_SESSION['erreur'] = 'Au moins un des champs est vide.';$vI->render(6);exit();}
    }else{$vI->render(6);exit();}

  }

  public function presentationItem($id){
    echo $id;
  }




}
