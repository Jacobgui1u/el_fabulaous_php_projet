<?php
namespace wishlist\controleur;
use wishlist\vue\Vue;
use wishlist\model\Membre;
use wishlist\model\Liste;
use wishlist\model\Item;

class ControleurListe{


  public function listeControle(){
      $vI=new Vue();
      $vI->render(2);
    }

/**
* methode privé qui verifie si on souhaite créer une nouelle liste
*/
private function nouvelleListe(){
    if (isset($_POST['newListe']) && $_POST['newListe'] == 'Nouvelle Liste') {
      header('Location: nouvelleListe');
      exit;
    }
}



/**
* on demande d'abord les informations avec un formulaire depuis créerFormulaire puis on recupere les données ici
* si on entre les bonnes choses, alors on est redirigé ici
* on créer un token dans l'url
* permettant avec l'url de retrouver la liste
* la liste a également un id utilisateur qui permet de lui etre associé avec un compte
* si les infomations rentré sont correcte, on créer une page contenant une liste vierge stocké dans la base
* le token est stocké de maniere crypté dans la bdd
*/
public function creerNouvelleListe(){
    date_default_timezone_set('Europe/Paris');
    //creation du token
    $token=$this->randToken();
    //cryptage du token pour stockage
    $tokenCrypt=password_hash($token,PASSWORD_BCRYPT);
    //on verifie si il y a un user de connecté
    $usid=array();
    $usid[0]=null;
    if (isset($_SESSION['login'] )) {
        //si oui on recupere son user_id
        $usid = Membre::select('id_membre')->where('login','=',$_SESSION['login'])->get();
        echo $_SESSION['login'];
        $usid= explode(":", $usid);
        $usid= explode("}", $usid[1]);
    }

  //on créer une nouvelle liste dans la bdd a partir des données entré dans le formulaire
  //no,user_id,titre,description,expiration,token
  $list=['user_id'=>$usid[0], 'titre'=>$_POST['titre'], 'description'=>$_POST['description'], 'expiration'=>$_POST['dateExpiration'], 'token'=>$tokenCrypt];
  $list=Liste::insert($list);

  //on a donc une page affichant une liste vierge

  //on head vers l'url de liste suivit du token
  header('Location: Liste/'.$token);
  exit();
}





/**
* methode pour verifier si les information de la liste sont correcte sinon demander d'entrer les infos
*/
public function creerFormulaire(){
  $_SESSION['erreur']=null;
  $vI=new Vue();
  if (isset($_POST['inscription']) && $_POST['inscription'] == 'Inscription') {
    if ((isset($_POST['titre']) && !empty($_POST['titre'])) && (isset($_POST['description']) && !empty($_POST['description'])) && (isset($_POST['dateExpiration']) && !empty($_POST['dateExpiration']))) {
      $this->creerNouvelleListe();
      $vI->render(5);exit();
      //on affiche le formulaire sans balise html
    }else{$_SESSION['erreur'] = 'Au moins un des champs est vide.';$vI->render(4);exit();}
  }else{$vI->render(4);exit();}
}



public function afficheListe($token){
  $vI=new Vue();
  $vI->tokenCourant($token);
  $this->deconnexion();
  //veut on ce connecter?
  $this->connexion($vI,5);
  $vI->render(5);
}



}
