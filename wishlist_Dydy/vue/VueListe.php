<?php
namespace wishlist\vue;
use wishlist\model\Membre;
use wishlist\model\Liste;
use wishlist\model\Item;


class VueListe{

private function formulaireGestionListe(){
  $res=$this->formulaireConnexion('./'.$this->token);
  $res=$res.<<<EOT
  <div id="ligne">
  </div>
  <form id="partBouton" action="" method="post">
    <input id='bouton' type="submit" name="modification" value="Modifier"><br />
    <input id='bouton' type="submit" name="public" value="Rendre publique"><br />
  </form>
EOT;
return $res;
}






/**
* fonction privé qui affiche une liste avec ses items et ses options
*/
private function liste(){
  $res=<<<EOT
  <body>
    <div class="part">
      <div id="ul">
      </div>
      <div id="entier">
        <h1>Items :</h1>

        <form action="../$this->token/item/" method="Post">
EOT;

  $token=null;
  $id=Liste::select('*')->get();
  foreach ($id as $tok ) {
    if(password_verify($this->token,$tok['token'])){

      $token=$tok['token'];
      break;
    }
  }
  $id=Liste::select('no')->where('token','=',$token)->get();

  $id=explode(":", $id);
  $id= explode("}", $id[1]);
  $item=Item::select('*')->where('liste_id','=',$id[0])->get();
  foreach($item as $i){
    $res=$res.'<input id="bnouvelleItem" type="submit" name="item" value='.$i['id'].'|'.$i['nom'].'><br />';
  }
$res=$res.<<<EOT
        </form>
        <form action="../$this->token/nouvelleItem" method="Post">
          <input id="bnouvelleItem" type="submit" name="newItem" value="Ajouter Item"><br />
        </form>
        <ul style="list-style-type:none">
        </ul>
      </div>
    </div>
    <div class="part">
      <div class="demi">
        <h1>copiez votre URL.</br> il servira a retrouver votre liste</h1>
EOT;
  $res=$res.$this->formulaireGestionListe();
  $res=$res.<<<EOT
      </div>
      <div class="demi">

      </div>
    </div>
EOT;
  return $res;
}







/*
  //créer une date seconde, minute, heure, jour, mois, année
  $d=strtotime($_POST['dateExpiration']);
  $date1=new \DateTime(date("Y-m-d ", $d));
  echo "Today is " . date("Y/m/d")."</br>" ;

  $date2=new \DateTime(date("Y/m/d"));
  $diff=date_diff($date1,$date2);
  echo "la date entre le : ".date("d/m/Y ", $d)." et ajourd'hui : ".date("d/m/Y")." est : </br>";
  echo $diff->format("Total number of days: %a.").'</br>';
  */
private function affichageListe(){
  return "<form id='f1' action='../wishlist' method='Post'>".
    '<li><input type="submit" name="liste1" value="l1"></li>'.
    "</form>";
}




/**
  * methode privé qui affiche un formulaire
  */
  private function formulaireListe(){
    $_POST['titre']=null;
    $_POST['description']=null;
    $_POST['dateExpiration']=null;
    $res=<<<EOT
    <h1>Creation de la liste :<br /></h1>
    <form id='formulaire' action="nouvelleListe" method="post">
      titre : </br> <input type="text" name="titre" value="" required="required"> <br />
      Description: </br><input type="text" name="description" value="" required="required"><br />
      Date d'expiration: </br><input type="date" name="dateExpiration" required="required"><br />
      <input id='inscription' type="submit" name="inscription" value="Inscription">
    </form>
EOT;

  if (isset($_SESSION['erreur'])){
    $res=$res.'<br/><h1 id=\'inscription\'> '.$_SESSION['erreur'].'</h1>';

  }
    return $res;
  }












}
