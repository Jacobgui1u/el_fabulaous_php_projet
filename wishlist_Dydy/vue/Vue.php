<?php
namespace wishlist\vue;
use wishlist\model\Membre;
use wishlist\model\Liste;
use wishlist\model\Item;
/**
* classe qui génére les pages html contenant du php
*/
class Vue{
  private $token;

  function Vue(){
    $token=null;
  }


  private function debutHtml($titre='wishlist',$style = NULL){
    $res='';
    $res = <<<EOT
    <!doctype html>
      <html lang="fr">
      <head>
        <meta charset="utf-8">
        <title>$titre</title>
        <link rel="stylesheet" href="$style">
      </head>
      <body>
EOT;
    return $res;
  }

  private function finHtml(){
    $res='';
    $res=<<<EOT
    </body>
  </HTML>
EOT;
  return $res;
  }

  /**
  *fonction qui donne un formulaire de connexion en html
  *$action est la position de la methode qui va utiliser les données entrée ici (soit la page)
  *$methode est la methode utilisé(soit post ou get etc...)
  */
  private function formulaireConnexion($action,$methode='post'){
    $_POST['login']=null;
    $_POST['pass']=null;
    $res='';
    if(isset($_SESSION['login'])){
      $res=$_SESSION['login']." est connecté </br>"/*.$_SESSION['token'].*/;
      $res=$res.<<<EOT
        <form action="$action" method="$methode">
        <br />
        <input type="submit" name="deconnexion" value="Deconnexion"><br />
      </form>
EOT;
    }else{

    //si on est connecté, on ecrit le nom et une possible deconnexion
    //, sinon, on ecrit la demande de connexion
    $res=<<<EOT
    <form action="$action" method="$methode">
    Login : <input type="text" name="login" value="" required="required"><br />
    Mot de passe : <input type="password" name="pass" value="" required="required"><br />
    <input type="submit" name="connexion" value="Connexion"><br />
    <a href="inscription" >Inscription </a>
    </form>
EOT;

      if (isset($_SESSION['erreur'])){
        $res=$res.'<br /><br />'.$_SESSION['erreur'];
      }
    }
    return $res;
  }







  private function index(){
    $res='';
      $res=$res.<<<EOT
      <body>
        <div class="part">
          <div id="ul">
          </div>
          <div id="entier">
            <h1>Mes listes:</h1>
            <form  action="nouvelleListe" method="Post">
                <input id="bNouvelleListe" type="submit" name="newListe" value="Nouvelle Liste"><br />
              </form>
              <ul style="list-style-type:none">
EOT;

//ici on recupere l'ensemble des liste de la personne
        $res=$res.'<form action="" method="POST">'.
          '<li><input type="submit" name="liste1" value="l1"></li>'.
        '</form>';
        $res=$res.<<<EOT
            </ul>
          </div>
        </div>
        <div class="part">
          <div class="demi">
EOT;
        $res=$res.$this->formulaireConnexion('').<<<EOT
          </div>
          <div class="demi">
            p
          </div>
        </div>
EOT;
      $res=$res.$this->finHtml();
      return $res;
    }




  private function inscription(){
    $_POST['login']=null;
    $_POST['pass']=null;
    $_POST['pass_confirm']=null;
    $res=<<<EOT
    Inscription à l'espace membre :<br />
    <form action="inscriptionDonnee" method="post">
    Login : <input type="text" name="login" value="" required="required"><br />
    Mot de passe : <input type="password" name="pass" value="" required="required"><br />
    Confirmation du mot de passe : <input type="password" name="pass_confirm" value="" required="required"><br />
    <input type="submit" name="inscription" value="Inscription">
    </form>
EOT;
    if (isset($_SESSION['erreur'])){
      $res=$res.'<br />'.$_SESSION['erreur'];
    }
    return $res;
  }



  public function tokenCourant($token){
    $this->token=$token;
  }




  public function render($choix){

    if($choix==1){
      echo $this->debutHtml('','src/css/style.css');
        echo $this->index();
      echo $this->finHtml();
    }elseif($choix==2){
      echo $this->debutHtml('','src/css/style.css');
        echo $this->affichageListe();
      echo $this->finHtml();
    }elseif($choix==3){
      echo $this->debutHtml('','src/css/style.css');
        echo $this->inscription();
      echo $this->finHtml();
    }elseif($choix==4){
      //affichage du formulaire pour creation de liste
      echo $this->debutHtml('','src/css/styleFormulaire.css');
        echo $this->formulaireListe();
      echo $this->finHtml();
    }elseif($choix==5){
      //on créer la page de liste
      //echo 'token courant url :  '.$this->token.'</br>';
      //on affiche la liste
      echo $this->debutHtml('','../src/css/styleListe.css');
      echo $this->liste();
      echo $this->finHtml();
    }elseif($choix==6){

      echo $this->debutHtml('','src/css/styleFormulaire.css');
      echo $this->formulaireItem();
      echo $this->finHtml();
    }

  }
}
