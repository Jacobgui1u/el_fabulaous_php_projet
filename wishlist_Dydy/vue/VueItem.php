<?php
namespace wishlist\vue;
use wishlist\model\Membre;
use wishlist\model\Liste;
use wishlist\model\Item;


class VueItem{

/**
  * methode privé qui affiche un formulaire
  */
  private function formulaireItem(){
    $_POST['nom']=null;
    $_POST['description']=null;
    $_POST['prix']=null;
    $_POST['url']=null;

    $res=<<<EOT
    <h1>Creation de la liste :<br /></h1>
    <form id='formulaire' action="nouvelleItem" method="post">
      Nom de l'item : </br> <input type="text" name="nom" value="" required="required"> <br />
      Description: </br><input type="text" name="description" value="" required="required"><br />
      Prix: </br><input type="number" name="prix" value="" required="required"><br />
      (non obligatoire) URL d'une image: </br><input type="text" name="url"><br />
      <input id='ajout' type="submit" name="ajouter" value="Ajouter">
    </form>
EOT;

  if (isset($_SESSION['erreur'])){
    $res=$res.'<br/><h4 id=\'inscription\'> '.$_SESSION['erreur'].'</h1>';

  }
    return $res;
  }






}
