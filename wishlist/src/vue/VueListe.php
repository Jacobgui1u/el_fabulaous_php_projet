<?php
namespace wishlist\vue;
use wishlist\model\Membre;
use wishlist\model\Liste;
use wishlist\model\Item;
use wishlist\model\Message;
use wishlist\model\Publique;
use wishlist\controleur\ControleurListe;

/**
* classe qui contient les methode utilisé pour l'affichage de ce qui est
* lié au Liste
*/
class VueListe{
  private $token;
  function __construct($tok){
    $this->token=$tok;
  }

  public function formulaireGestionListe(){
    $app = \Slim\Slim::getInstance();
    $pageCo=new VueConnexion();
    $res=$pageCo->formulaireConnexion("./../");
    $res=$res."</div>";
      $res.='<div id="boutoncont">';
      $res.='<a id="bouton" href="'.$app->urlFor('modification',array('token' => $this->token)).'">Modifier</a>';

      $contListe=new ControleurListe();
      //bloc condition, si il existe une liste publique au token en parametre
      if(!$contListe->verifPublique($this->token)){
        $res.='<a id="bouton" href="'.$app->urlFor('public',array('token' => $this->token)).'">Rendre publique</a>';
      }else{
        $res.='<h3 id="bouton1"> liste deja publique </h3>';
      }
      $res.="</div>";
    return $res;
  }

  /**
  * methode privé qui affiche un formulaire
  */
  public function formulaireListe(){
    $_POST['titre']=null;
    $_POST['description']=null;
    $_POST['dateExpiration']=null;
    $res=<<<EOT
    <h1>Creation de la liste :<br /></h1>
    <form id='formulaire' action="nouvelleListe" method="post">
      titre : </br> <input type="text" name="titre" value="" required="required"> <br />
      Description: </br><input type="text" name="description" value="" required="required"><br />
      Date d'expiration: </br><input type="date" name="dateExpiration" required="required"><br />
      <input id='inscription' type="submit" name="inscription" value="Inscription">
    </form>
EOT;

  if (isset($_SESSION['erreur'])){
    $res=$res.'<br/><h1 id=\'inscription\'> '.$_SESSION['erreur'].'</h1>';

  }
    return $res;
  }


  /**
  * fonction privé qui affiche une liste avec ses items et ses options
  */
  public function liste($token=null){
    $co=false;
    //block verification connexion
    if(isset($_SESSION['login']) && !empty($_SESSION['login'])){
      $id=Membre::select('id_membre')->where('login','=',$_SESSION['login'])->get();
      $id=json_decode($id);
      if(count($id)==1){
        $co=true;
      }
    }

    //block qui verifie si le token est bon
    $pass=true;
    if(isset($token)){
      $this->token=$token;
    }
    //block init code  html
    $app = \Slim\Slim::getInstance();
    $res=<<<EOT
    <body>
      <div class="part">
        <div id="ul">
        </div>
        <div id="entier">
EOT;
      $res.='<a id="retour" href="'.$app->urlFor('acceuil').'">retour</a>';
      $res.=<<<EOT
      <h5>reservation(rouge réservé)</5>
        <h1>Items :</h1>
EOT;


    //on recupere les listes appartenant au token
    $id=Liste::select('no')->where('token','=',$this->token)->get();
    //bloc extraction
    $id=explode(":", $id);
    $id= explode("}", $id[1]);
    $id=$id[0];

    $resa=false;
    //bloc recuperation des item
    $item=Item::select('*')->where('liste_id','=',$id)->get();
    $res.="<div id=liste>";
    //on recherche tout les morceau voulu (titre etc..)qu'on ajoute dans un composant
    foreach($item as $i){
      $res.='<div id=contenant>';
      //bloc reservation
      $i['reserve'];
      if($i['reserve']){
        $res.='<div id=r></div>';
        $resa=true;
      }else{
        $res.='<div id=v></div>';
        $resa=false;
      }
      //bloc lien item
      $res.='<a id="bnouvelItem"  href="'.$app->urlFor('showItem', array('token' => $this->token, 'idItem' => $i['id'])).'">'.$i['nom'].'</a>';


      /*----------------------------------*/
      if(isset($_COOKIE['createur'])){
        foreach($_COOKIE['createur'] as $c){
              if($c==$this->token){
                $pass=false;

            }
          }
      };
    /*----------------------------------*/

        //bloc message
        $mess=Message::select('*')->where('idItem','=',$i['id'])->get();
        foreach($mess as $mi){
          if($pass){
            $res.='<div id="info">'.$mi['nomPart'].'</div>';
            $res.='<div id="info">'.$mi['message'].'</div>';
          }
        }
        if($pass && !$resa && !$co){
          $res.='<a id="reserver"  href="'.$app->urlFor('afficheReserver', array('token' => $this->token, 'idItem' => $i['id'])).'">reserver</a>';
        }

      $res.='</div>';

    }
    if(!$pass){
      $res.='<a id="bnouvelItem1"  href="'.$app->urlFor('formulaireItem',array('token' => $this->token )).'">Ajouter Item</a>';
    }
      $res.='</div>'.<<<EOT
    </div>
  </div>
  <div class="part">
    <div id="demiHaut">
      <div id="demidemi">
        <h1>copiez votre URL.</br> il servira a retrouver votre liste</h1>
EOT;
   $res=$res.$this->formulaireGestionListe();
    $res=$res.<<<EOT
    </div>

  <div id="demiBas">
    p
  </div>
  </div>
EOT;
    return $res;
  }
}
