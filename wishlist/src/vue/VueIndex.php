<?php
namespace wishlist\vue;
use wishlist\model\Publique;
use wishlist\model\Membre;
use wishlist\model\Liste;
use wishlist\model\Item;
use wishlist\controleur\ControleurListe;

/**
* classe qui contient les methode utilisé pour l'affichage de l'index
*/
class VueIndex{

  public function index(){
    $res='';
      $res=$res.<<<EOT
      <body>
        <div class="part">
          <div id="ul">
          </div>
          <div id="entier">
            <h1>Mes listes:</h1>
EOT;

      $app = \Slim\Slim::getInstance();
      $res.='<div id=bneoL><a  id="bNouvelleListe"  href="'.$app->urlFor('new').'">'."Nouvelle Liste".'</a>';

      $res.="</div><div id='liste'>";



      $contListe=new ControleurListe();
      //bloc si connecté, ses listes suivis des publics, sinon juste les publics
      if(isset($_SESSION['login'])&& !empty($_SESSION['login'])){
        $idus=Membre::select('id_membre')->where('login','=',$_SESSION['login'])->get();
        $idus=explode("}",explode(':',$idus)[1])[0];
        //ici on recupere l'ensemble des liste de la personne
        $list=Liste::select('*')->where('user_id',"=",$idus)->get();

        if(!empty($list)){
          //bloc liste personne
          foreach($list as $i){
            $chaine=str_split($i['titre']);
            $text='';
            $count=0;
            //bloc agancement des mots dans les boutons
            foreach($chaine as $c){
              if($c==' '){$count++;}
              if($count==3){$text=$text."</br>";$count=0;}
              $text=$text.$c;
            }

            //bloc creation boutons
            $app=\Slim\Slim::getInstance();
            $res.='<a class="boutonliste" href="'.$app->urlFor('Liste',array('token' => $i['token'] )).'">'.$text.'</a>';

            }
        }
      }
      //bloc des liste associé au cookie createur
      if(isset($_COOKIE['createur']) ){
        foreach($_COOKIE['createur'] as $c){
          $l=Liste::where('token','=',$c)->first();
          $res.='<a class="boutonliste" href="'.$app->urlFor('Liste',array('token' => $c )).'">'.$l->titre.'</a>';

        }
      };

      //bloc liste publique
      $res.='<h1>Listes </br> publiques</h1>';
        $listpub=Publique::All();
        foreach($listpub as $lp){
          $list=Liste::select('*')->where('no','=',$lp['noList'])->get();
          //$list=Liste::select('*')->get();
          foreach($list as $i){
            $chaine=str_split($i['titre']);
            $text='';
            $count=0;
            foreach($chaine as $c){
              if($c==' '){$count++;}
              if($count==3){$text=$text."</br>";$count=0;}
              $text=$text.$c;
            }
            //affichage des listes publique
            $app=\Slim\Slim::getInstance();
            $res.='<a class="boutonliste" href="'.$app->urlFor('Liste',array('token' => $i['token'] )).'">'.$text.'</a>';
          }
        }
      $res.=<<<EOT
          </div>
        </div>
      </div>
      <div class="part">
        <div class="demi">
EOT;
      $pageCo=new VueConnexion();
      $res=$res.$pageCo->formulaireConnexion("./").<<<EOT
        </div>
        <div class="demi">
          p
        </div>
      </div>
EOT;
      return $res;
    }
  }
