<?php
namespace wishlist\vue;
use wishlist\model\Membre;
use wishlist\model\Liste;
use wishlist\model\Item;


/**
* classe qui contient les methode utilisé pour l'affichage de ce qui est
* lié au Item
*/
class VueItem{
  private $token;
  private $item;
  /**
  * methode privé qui affiche un formulaire
  */
  public function formulaireItem(){
    $_POST['nom']=null;
    $_POST['description']=null;
    $_POST['prix']=null;
    $_POST['url']=null;

    $res=<<<EOT
    <h1>Creation de l'Item :<br /></h1>
    <form id='formulaire' action="finAJout" method="post">
      Nom de l'item : </br> <input type="text" name="nom" value="" required="required"> <br />
      Description: </br><input type="text" name="description" value="" required="required"><br />
      Prix: </br><input type="number" name="prix" value="" required="required"><br />
      (non obligatoire) URL d'une image: </br><input type="text" name="url"><br />
      <input id='ajout' type="submit" name="ajouter" value="Ajouter">
    </form>
EOT;

  if (isset($_SESSION['erreur'])){
    $res=$res.'<br/><h4 id=\'inscription\'> '.$_SESSION['erreur'].'</h1>';

  }
    return $res;
  }


  public function affichageItem($token,$item){
    $this->token=$token;
    $this->item=$item;
    $res="";
    $res.=<<<EOT
    <body>
      <div class="demi">
EOT;
      $app=\Slim\Slim::getInstance();
      $res.='<a id="retour" href="'.$app->urlFor('Liste', array('token' => $this->token)).'">retour</a>';
      $res.=<<<EOT
        <header>
        <p>nom</p>
        </header>
        <section id="s1">
          <div class="part">
            <div id="img">
            </div>
          </div>
          <div class="part">
            <div id="description">
              <h3>description :</h3>
            </div>
            <div id="prix">
              <h3>prix :</h3>
            </div>
          </div>
        </section>
      </div>
      <div class="demi">
EOT;
    $pageCo=new VueConnexion();
    $res.=$pageCo->formulaireConnexion("./../../../");
    $res.="</div>";

    return $res;
  }

  public function affichageReservation($token,$id){
    $_POST['idItem']=$id;
    $_POST['nomPart']=null;
    $_POST['message']=null;

    $res=<<<EOT
    <form id='formulaire' action="reservation" method="post">
      Nom du participant: </br> <input type="text" name="nomPart" value="" required="required"> <br />
      Message: </br><input type="text" name="message" value="" required="required"><br />
      <input id='ajout' type="submit" name="ajouter" value="Ajouter">
    </form>
EOT;
return $res;
  }
}
