<?php
namespace wishlist\vue;
use wishlist\model\Membre;
use wishlist\model\Liste;
use wishlist\model\Item;

/**
* classe qui contient les methode utilisé pour l'affichage de ce qui est
* lié au compte (inscription, connexion)
*/
class VueConnexion{


  public function inscription(){
    $_POST['login']=null;
    $_POST['pass']=null;
    $_POST['pass_confirm']=null;
    $res=<<<EOT
    Inscription à l'espace membre :<br />
    <form action="inscriptionDonnee" method="post">

    Login : <input type="text" name="login" value="" required="required"><br />
    Mot de passe : <input type="password" name="pass" value="" required="required"><br />
    Confirmation du mot de passe : <input type="password" name="pass_confirm" value="" required="required"><br />
    <input type="submit" name="inscriptionDonnee" value="Inscription">
    </form>
EOT;
    if (isset($_SESSION['erreur'])){
      $res=$res.'<br />'.$_SESSION['erreur'];
    }
    return $res;
  }


  /**
  *fonction qui donne un formulaire de connexion en html
  *$action est la position de la methode qui va utiliser les données entrée ici (soit la page)
  *$methode est la methode utilisé(soit post ou get etc...)
  */
    public function formulaireConnexion($url,$methode="post"){
    $_POST['login']=null;
    $_POST['pass']=null;
    $res='';

    if(isset($_SESSION['login'])){
      $app=\Slim\Slim::getInstance();
      $res.=$_SESSION['login']." est connecté </br>"/*.$_SESSION['token'].*/;

      $res.='<a href="'.$app->urlFor('deconnexion').'">Deconnexion</a>';


    }else{

    //si on est connecté, on ecrit le nom et une possible deconnexion
    //, sinon, on ecrit la demande de connexion
    $app=\Slim\Slim::getInstance();
    $res.='<form action="'.$url.'connexion" method="'.$methode.'">';
    $res.=<<<EOT
    Login : <input type="text" name="login" value="" required="required"><br />
    Mot de passe : <input type="password" name="pass" value="" required="required"><br />
    <input type="submit" name="connexion" value="Connexion"><br />
    </form>
EOT;
  /*  $req = $app->request();
    $uri = $req->getUrl() . $req->getRootUri();*/
    $res.='<a href="'.$app->urlFor('affichageInscription').'">Inscription</a>';
      if (isset($_SESSION['erreur'])){
        $res=$res.'<br /><br />'.$_SESSION['erreur'];
      }
    }
    return $res;
  }
}
