<?php
namespace wishlist\vue;
use wishlist\model\Membre;
use wishlist\model\Liste;
use wishlist\model\Item;

/**
* classe qui génére les pages html contenant du php
*/
class Vue{
  private $token;
  private $item;

  /**
  * constructeur qui initie l'attribut token a null
  */
  function __construct(){
    $this->token=null;
    $this->item=null;
  }

  /**
  * methode qui permet de debutter une page Html
  */
  private function debutHtml($titre='wishlist',$style = NULL){
    //bloc init
    $res='';
    //heredoc
    $res = <<<EOT
    <!doctype html>
      <html lang="fr">
      <head>
        <meta charset="utf-8">
        <title>$titre</title>
        <link rel="stylesheet" href="$style">
      </head>
      <body>
EOT;
    return $res;
  }

  /**
  * methode qui permet de finir une page html
  */
  private function finHtml(){
    //bloc init
    $res='';
    //heredoc
    $res=<<<EOT
    </body>
  </HTML>
EOT;
  return $res;
  }

  /**
  * methode qui permet de mettre a jour l'attribut token
  */
  public function tokenCourant($token){
    $this->token=$token;
  }

  /**
  * methode qui permet de mettre a jour l'attribut item
  */
  public function itemCourant($id){
    $this->item=$id;
  }
  /**
  * methode qui permet de rendre une page en fonction du choix
  * $choix du rendu qu'on souhaite avoir
  */
  public function render($choix){
    //affichage de l'index
    if($choix==1){
      $pageIn=new VueIndex();
      echo $this->debutHtml('','./css/style.css');
        echo $pageIn->index();
      echo $this->finHtml();
    }
    //affichage de la page d'inscription (creation d'un compte)
    elseif($choix==2){
      $pageCo=new VueConnexion();
      echo $this->debutHtml('','./css/style.css');
        echo $pageCo->inscription();
      echo $this->finHtml();
    }
    //affichage du formulaire pour creation de liste
    elseif($choix==3){
      $pageLi=new VueListe($this->token);
      echo $this->debutHtml('','./css/styleFormulaire.css');
        echo $pageLi->formulaireListe();
      echo $this->finHtml();
    }
    //affichage d'une liste
    elseif($choix==4){
      $pageLi=new VueListe($this->token);
      echo $this->debutHtml('','./../css/styleListe.css');
        echo $pageLi->liste($this->token);
      echo $this->finHtml();
    }

    elseif($choix==5){
      $pageIt=new VueItem();
      echo $this->debutHtml('','./css/styleFormulaire.css');
        echo $pageIt->formulaireItem();
      echo $this->finHtml();
    }elseif($choix==6){
      $pageIt=new VueItem();
      echo $this->debutHtml('','./../../../css/styleItem.css');
        echo $pageIt->affichageItem($this->token,$this->item);
      echo $this->finHtml();
    }elseif($choix==7){
      $pageIt=new VueItem();
      echo $this->debutHtml('','./../../../css/styleItem.css');
        echo $pageIt->affichageReservation($this->token,$this->item);
      echo $this->finHtml();
    }

  }
}
