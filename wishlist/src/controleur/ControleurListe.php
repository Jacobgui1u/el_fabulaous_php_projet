<?php
namespace wishlist\controleur;
use wishlist\vue\Vue;
use wishlist\model\Membre;
use wishlist\model\Liste;
use wishlist\model\Item;
use wishlist\model\Publique;


/**
* classe qui gere les Listes de leur
* insertion a leur affichage en passant par leur controle
*/
class ControleurListe{
  /**
  * methode pour verifier si les information de la liste sont correcte
  * sinon demander d'entrer les infos
  */
  public function creerFormulaire(){
    //bloc init
    $_SESSION['erreur']=null;
    $vI=new Vue();
    //bloc verification clique inscription et bon remplissage des champs utils (non vide)
    if (isset($_POST['inscription']) && $_POST['inscription'] == 'Inscription') {
      if ((isset($_POST['titre']) && !empty($_POST['titre'])) && (isset($_POST['description']) && !empty($_POST['description'])) && (isset($_POST['dateExpiration']) && !empty($_POST['dateExpiration']))) {
        //bloc creation d'une nouvelle liste et rendu de celle ci
        $this->creerNouvelleListe();
        //bloc erreur champ vide
      }else{$_SESSION['erreur'] = 'Au moins un des champs est vide.';$vI->render(3);exit();}
      //bloc appel affichage formulaire
    }else{$vI->render(3);exit();}
  }


  /**
  * methode qui créer une nouvelleListe avec son token et redirige vers elle
  * on demande d'abord les informations avec un formulaire depuis créerFormulaire puis on recupere les données ici
  * si on entre les bonnes choses, alors on est redirigé ici
  * on créer un token dans l'url
  * permettant avec l'url de retrouver la liste
  * la liste a également un id utilisateur qui permet de lui etre associé avec un compte
  * si les infomations rentré sont correcte, on créer une page contenant une liste vierge stocké dans la base
  * le token est stocké de maniere crypté dans la bdd
  */
  public function creerNouvelleListe(){
    //bloc init et setteur de l'heure francaise
    date_default_timezone_set('Europe/Paris');
    $token=$this->randToken();
    $usid=null;

    //bloc verification si il y a un utilisateur connecté
    if (isset($_SESSION['login'] )) {
      $usid = Membre::select('id_membre')
                      ->where('login','=',$_SESSION['login'])
                      ->get();
      $usid= explode(":", $usid);
      $usid= explode("}", $usid[1]);
      $usid=$usid[0];
    }
    /*----------------------------------*/

    $a = strptime($_POST['dateExpiration'], '%Y-%m-%j');
    $h1 = mktime(0, 0, 0, $a['tm_mon']+1, $a['tm_mday'], $a['tm_year']+1900);
    $tday = time();
    $h2 = mktime(0, 0, 0, $a['tm_mon']+1, $a['tm_mday'], $a['tm_year']+1900);

    echo date("d-m-Y",$h2).'</br>';
    echo date("d-m-Y",$tday).'</br>';
    setCookie('createur[\''.$token.'\']',$token,(time()+$h2-$tday));

    //bloc creation et insertion d'une nouvelle liste
    $list=['user_id'=>$usid,
     'titre'=>$_POST['titre'],
      'description'=>$_POST['description'],
        'expiration'=>$_POST['dateExpiration'],
          'token'=>$token];

    $list=Liste::insert($list);
    //bloc redirection vers liste vierge
    $app=\Slim\Slim::getInstance();
    $app->redirect("Liste/".$token);
    /*----------------------------------*/
  }


  /**
  * methode privé qui créer un token très aleatoire pour l'url
  */
  private function randToken(){
    $token=openssl_random_pseudo_bytes(32);
    $token=bin2hex($token);
    return $token;
  }

  /**
  * methode qui va afficher une liste en fonction de son token
  * $token : token de la liste
  */
  public function afficheListe($token){
    $vI=new Vue();
    $vI->tokenCourant($token);
    /*TODO*/
    $listCompte=new ControleurCompte();
    $vI->render(4);
  }

  /**
  * methode qui rend publique une liste, en cherchant son id
  * et en l'ajoutant a la liste des publique
  * $token : token de la liste
  */
  public function rendrePublique($token){

    $idL=Liste::select('no')->where('token','=',$token)->get();
    $idL=explode("}",explode(':',$idL)[1])[0];
    $idL=['noList'=>$idL];
    Publique::insert($idL);
    $app=\Slim\Slim::getInstance();
    $app->redirect("../".$token);
  }

  /**
  * methode qui verifie si une liste est publique
  * $token le token de la liste a verifier
  */
  public function verifPublique($token){
    //bloc recherche de la liste
    $idL=Liste::select('no')->where('token','=',$token)->get();
    $idL=explode("}",explode(':',$idL)[1])[0];
    //bloc verification liste publique
    $pub=Publique::select('*')->where('noList','=',$idL)->get();
    //bloc de recherche
    $count=count($pub);
    $res=false;

    //il existe une liste publique, donc retour vrai
    if($count!=0){
        $res=true;
    }

    //bloc return
    return $res;


  }

}
