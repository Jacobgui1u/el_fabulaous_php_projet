<?php
namespace wishlist\controleur;
use wishlist\vue\Vue;
use wishlist\model\Membre;
use wishlist\model\Liste;
use wishlist\model\Item;

/**
* classe qui gere l'acceuil
* de son affichage a ses controle
*/
class ControleurAcceuil{

  /**
  * methode qui va afficher la
  * page d'index du site a l'arrivé
  * ainsi que la creation d'une nouvelle liste si demandé
  */
  public function acceuil(){
    $vI=new Vue();
    if (isset($_POST['newListe']) && $_POST['newListe'] == 'Nouvelle Liste') {
      $app->redirect("nouvelleliste");
    }
    if(isset($_SESSION['login'])){
      $result = Membre::select('login')->where( 'login','=',$_SESSION['login'])->get();
      // si on obtient une réponse, alors l'utilisateur est un membre
      $count=0;
      foreach($result as $log){
        $count=$count+1;
      }

      if ($count==0) {
        $_SESSION['login']=null;
      }
    }
    $vI->render(1);
  }


}
