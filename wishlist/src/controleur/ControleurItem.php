<?php
namespace wishlist\controleur;
use wishlist\vue\Vue;
use wishlist\model\Membre;
use wishlist\model\Liste;
use wishlist\model\Item;
use wishlist\model\Message;
use wishlist\model\Publique;

/**
* classe qui gere les Items de leur
* insertion a leur affichage en passant par leur controle
*/
class ControleurItem{


  /**
  * fonction qui créer et ajoute un item dans la base de donnée
  * a partir d'une liste
  * $token de la liste qui contient les items
  */
  private function nouveauItem($token){
    //bloc recherche de l'id de la liste associé au token
    $idL=Liste::select('no')->where('token','=',$token)->get();
    $idL=explode("}",explode(':',$idL)[1])[0];

    /////////////////////////////////////////////
    //bloc qui créer et insert l'item (verification si une url est ajouté au préalable)
    if(isset($_post['url'])){
      $it=['liste_id'=>$idL ,
        'nom'=>$_POST['nom'],
         'descr'=>$_POST['description'],
          'tarif'=>$_POST['prix'],
           'url'=>$_POST['url']];
      $it=Item::insert($it);
    }else{
      $it=['liste_id'=>$idL ,
        'nom'=>$_POST['nom'],
         'descr'=>$_POST['description'],
          'tarif'=>$_POST['prix'],
            'url'=>null];
      $it=Item::insert($it);
    }

  }

  /**
  * methode qui ajoute un item dans la liste courante
  * verification du clique d'ajout et des champs de l'item (non vide) et sécuritaire
  * puis ajout de l'item avec 'nouveauItem'
  * enfin redirection vers la page de l'item
  * $token d'identification de la liste courante
  */
  public function ajoutItem($token){
    //bloc init
    $max=0;
    $_SESSION['erreur']=null;
    $idIt=null;
    $vI=new Vue();
    $vI->tokenCourant($token);
    //bloc vérification
    if (isset($_POST['ajouter']) && $_POST['ajouter'] == 'Ajouter') {
      if ((isset($_POST['nom']) && !empty($_POST['nom'])) && (isset($_POST['description']) && !empty($_POST['description']))&& (isset($_POST['prix']) && !empty($_POST['prix']))) {
        //bloc creation item
        $this->nouveauItem($token);
        //recherche du dernier item créer (celui qui vient d'etre créer par nouveauItem)
        $a=Item::select('*')->get();
        foreach($a as $i){
          $idIt=$i['id'];
        }
        //bloc redirection
        if(isset($idIt)){
          $_post['ajouter']=null;
          $app=\Slim\Slim::getInstance();
          $app->redirect("../".$token.'/item/'.$idIt);
        }
        //bloc erreur, pas d'item trouvé
        else{
          $_post['ajouter']=null;
          $_SESSION['erreur'] = 'erreur de creation, pas d\'id généré.';$vI->render(5);exit();
        }
      }
      //bloc erreur champ vide
      else{$_SESSION['erreur'] = 'Au moins un des champs est vide.';$vI->render(5);exit();}
    }
    //bloc affichage origine
    else{$vI->render(5);exit();}
  }



  /**
  * methode d'affichage de l'item
  */
  public function presentationItem($id,$token){
    $vIt=new Vue();
    $vIt->tokenCourant($token);
    $vIt->itemCourant($id);
    $vIt->render(6);
  }

  public function formulaireReservation($token,$id){
    $vIt=new Vue();
    $vIt->tokenCourant($token);
    $vIt->itemCourant($id);
    $vIt->render(7);
  }

  public function traitementReservation($token,$i){
    $app=\Slim\Slim::getInstance();
    //bloc init
    $_SESSION['erreur']=null;
    $vI=new Vue();
    //bloc vérification
    if (isset($_POST['ajouter']) && $_POST['ajouter'] == 'Ajouter') {
      if ((isset($_POST['nomPart']) && !empty($_POST['nomPart'])) && (isset($_POST['message']) && !empty($_POST['message']))) {
        //verification de non duplicité
        $mss=Message::select('*')->where('idItem','=',$i)->get();
        $count=0;
        foreach($mss as $m){
          $count++;
        }
        if($count==0){
        //bloc reservation



          //bloc modification en reservation
          $it=Item::find($i);
          $it->reserve=1;
          $it->save();
          //bloc redirection

          $it=['idItem'=>$i ,
             'nomPart'=>$_POST['nomPart'],
              'message'=>$_POST['message']];
           $it=Message::insert($it);

          $_post['ajouter']=null;

          $app->redirect("../../../".$token);

        }else{
          $_SESSION['erreur'] = 'commentaire deja existant';
          $vI->tokenCourant($token);
          $app->redirect("../../../".$token);
          exit();
        }


    }
    //bloc erreur champ vide
    else{$_SESSION['erreur'] = 'Au moins un des champs est vide.';  $app->redirect("../../../".$token);exit();}
  }
    //bloc affichage origine
  else{$vI->render(4);exit();}

  }

}
