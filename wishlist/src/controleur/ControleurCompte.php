<?php
namespace wishlist\controleur;
use wishlist\vue\Vue;
use wishlist\model\Membre;
use wishlist\model\Liste;
use wishlist\model\Item;
/**
* classe qui gere les controle de l'affichage
*/
class ControleurCompte{


  /**
  * methode qui nous deconnecte
  */
  public function deconnexion(){
    $_SESSION['login']=null;
    $app=\Slim\Slim::getInstance();
    $app->redirect("./");
  }

  /**
  * methode permettant de gerer les données de connexion
  * on y test si on submit les donnée
  * puis si login et mdp sont renseigné et non vide
  * enfin si on trouve un compte dans la bdd, on stock dans la variable Session le login
  *
  * $vI est la vue courante dans laquelle on sera rediriger après la connexion
  * $rendu est l'option de rendu que l'on souhaite (la page dans render)
  */
  public function connexion(){
    //bloc init
    $nbCompteTrouve=0;
    $_SESSION['erreur']=null;
    //bloc verification et recherche du compte
    if (isset($_POST['connexion']) && $_POST['connexion'] == 'Connexion') {
      if ((isset($_POST['login']) && !empty($_POST['login']))
                && (isset($_POST['pass']) && !empty($_POST['pass']))) {

        $liste = Membre::select('*')
                        ->where( 'login','=',$_POST['login'])
                        ->get();
        foreach ($liste as $mdp ) {
          if(password_verify($_POST['pass'],$mdp['mdpCrypt'])){
            $nbCompteTrouve++;

            break;
          }
        }
        echo '<pre>';
        var_dump($_POST);
        //bloc reponse positive
        if ($nbCompteTrouve==1) {
          $_SESSION['login'] = $_POST['login'];

          $app=\Slim\Slim::getInstance();
          $app->redirect("../wishlist");

        }
        //bloc 'pas de compte' trouvé
        elseif ($nbCompteTrouve== 0) {
          $_SESSION['erreur'] = 'Compte non reconnu.';
          $app=\Slim\Slim::getInstance();
          $app->redirect("./");
        }
        //bloc 'trop de compte' trouvé
        else {
          $_SESSION['erreur']= 'Problème dans la base de données : plusieurs membres ont les mêmes identifiants de connexion.';
          $app=\Slim\Slim::getInstance();
          $app->redirect("./");
        }
      }else {
        $_SESSION['erreur']= 'Au moins un des champs est vide.';
        $app=\Slim\Slim::getInstance();
        $app->redirect("./");
      }
    }
  }

  /**
  * methode qui va permettre l'affichage
  * de la page d'inscription pour créer un compte
  * elle fait pour cela appel a render de vue
  * (3eme)
  */
  public function AfficheInscription(){
    $vI=new Vue();
    $vI->render(2);
  }



  /**
  * table create table membre( id_membre int NOT NULL AUTO_INCREMENT, login varchar(32), mdpCrypt varchar(255),primary key(id_membre));
  * methode qui permet l'inscription a partir des
  * informations donnée dans la page d'inscription
  * On va tester si le visiteur clique sur le bouton et si il a correctement
  * rempli les champs (de maniere non vide)
  *
  *
  *
  *
  */
  public function inscription(){
    //bloc init
    $_SESSION['erreur']=null;
    //bloc verification
    if (isset($_POST['inscriptionDonnee']) && $_POST['inscriptionDonnee'] == 'Inscription') {

      if ((isset($_POST['login']) && !empty($_POST['login']))&& (isset($_POST['pass']) && !empty($_POST['pass']))&& (isset($_POST['pass_confirm']) && !empty($_POST['pass_confirm']))) {
              /*TODO*/
              /* on testera plus tard si il y a des caractere non voulue */
              /*TODO*/

        if ($_POST['pass'] != $_POST['pass_confirm']) {

          $_SESSION['erreur'] = 'Les 2 mots de passe sont différents.';
          $app=\Slim\Slim::getInstance();
          $app->redirect("affichageInscription");
        }
        //bloc unicité compte
        else {

          $result = Membre::select('login')->where('login','=',$_POST['login'])->get();

          if (count($result) == 0) {

            //bloc creation et insertion compte avec cryptage du message
            $mb=['login'=>$_POST['login'],
              'mdpCrypt'=>password_hash($_POST['pass'],PASSWORD_BCRYPT)];
            Membre::insert($mb);
            //bloc redirection

            $app=\Slim\Slim::getInstance();
            $app->redirect("./");
          }
          //bloc erreur unicité
          else {
            $_SESSION['erreur'] = 'Un membre possède déjà ce login.';
            $app=\Slim\Slim::getInstance();
            $app->redirect("affichageInscription");
          }
        }
        //bloc mauvais remplissage des champs
      }else {
        $_SESSION['erreur'] = 'Au moins un des champs est vide.';
        $app=\Slim\Slim::getInstance();
        $app->redirect("affichageInscription");
      }
    }
  }


}
