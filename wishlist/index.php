<?php
require_once 'vendor/autoload.php';
use \Illuminate\Database\Capsule\Manager as DB;
use \wishlist\controleur\ControleurAcceuil;
use \wishlist\controleur\ControleurCompte;
use \wishlist\controleur\ControleurItem;
use \wishlist\controleur\ControleurListe;

session_start();
//bloc de connexion a la base de donnée a partir d'un fichier conf.ini
$db=new DB();
$db->addConnection(parse_ini_file('src/conf/conf.ini'));
$db->setAsGlobal();
$db->bootEloquent();

//bloc routeur
$app= new \Slim\Slim();

/**
* index
* affichage de la page d'acceuil
*/
$app->get('/',function(){
  $cf = new ControleurAcceuil();
  $cf->acceuil();
})->name('acceuil');


/**
* index
* recuperation des informations de la page d'acceuil
*/
$app->post('/',function(){
  $cf = new ControleurAcceuil();
  $cf->acceuil();
});


$app->post('/connexion',function(){
  $cf = new ControleurCompte();
  $cf->connexion();
})->name('connexion');


$app->get('/deconnexion',function(){
  $cf = new ControleurCompte();
  $cf->deconnexion();
})->name('deconnexion');


/**
* inscription
* affichage de la page d'inscription au site (en vue de creation d'un compte)
*/
$app->get('/affichageInscription',function(){
  $cf = new ControleurCompte();
  $cf->AfficheInscription();
})->name('affichageInscription');

/**
* inscription
* récuperation des données de l'inscription et inscription au site (creation d'un compte)
*/
$app->post('/inscriptionDonnee',function(){
  $cf=new ControleurCompte();
  $cf->inscription();
})->name('inscriptionDonnee');


/**
* creation Liste
* affichage du formulaire de creation de liste
* et recuperatin des données du formulaire
*/
$app->get('/nouvelleListe',function(){
  $cf = new ControleurListe();
  $cf->creerFormulaire();
})->name("new");

$app->post('/nouvelleListe',function(){
  $cf = new ControleurListe();
  $cf->creerFormulaire();
});


/**
* Liste
* affichage d'une liste
* $id token de la liste
*/
$app->get('/Liste/:token',function($token){
  $cf=new ControleurListe();
  $cf->afficheListe($token);
})->name('Liste');


/**
* Liste
* recuperation des informations fourni a une liste (clique sur un item etc...)
* $id token de la liste
*/
$app->post('/Liste/:token',function($token){
  $cf=new ControleurListe();
  $cf->afficheListe($token);
});

/**
* ajout d'un item dans une liste
* token de la liste d'ajout
*/
$app->get('/Liste/:token/nouvelItem',function($token){
  $cf=new ControleurItem();
  $cf->ajoutItem($token);
})->name('formulaireItem');

/**
* ajout d'un item dans une liste
* token de la liste d'ajout
*/
$app->post('/Liste/:token/finAJout',function($token){
  $cf=new ControleurItem();
  $cf->ajoutItem($token);
});

/*
*
*/
$app->get('/:token/item/',function($token){
  $app->redirect(explode("|",$_POST['item'])[0]);
  exit();
});


/*
* Item
* affichage d'un item appartenant a une liste
* $token token d'identification de la liste
* $idItem id de l'item
*/
$app->get('/Liste/:token/item/:idItem',function($token,$idItem){

  $cf=new ControleurItem();
  $cf->presentationItem($idItem,$token);
})->name('showItem');





/*
* Liste
* modification d'une liste
* $token token d'identification de la liste
*/
$app->get('/Liste/:token/modif',function($token){
  echo 'ok';
})->name('modification');


/*
* Liste
* mise en public d'une liste
* $token token d'identification de la liste
*/
$app->get('/Liste/:token/public',function($token){
  $cf=new ControleurListe();
  $cf->rendrePublique($token);
})->name('public');

/*
* Item
* permet de reserver un item en remplissant un formulaire
* $token token d'identification de la liste
* $idItem id de l'item
*/
$app->get('/Liste/:token/item/:idItem/reserver',function($token,$idItem){

  $cf=new ControleurItem();
  $cf->formulaireReservation($token,$idItem);

})->name('afficheReserver');

/*
* Item
* traite la reservation
* $token token d'identification de la liste
* $idItem id de l'item
*/
$app->post('/Liste/:token/item/:idItem/reservation',function($token,$idItem){

  $cf=new ControleurItem();
  $cf->traitementReservation($token,$idItem);

})/*->name('reserver')*/;




//on lance le routeur
$app->run();
